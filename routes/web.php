<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/** Página inicial dos usuários */
Route::get('/', 'UserController@dashboard')->name('user.dashboard');
/** Rota de filtrar a listagem */
Route::match(['post', 'get'], '/filtro', 'UserController@filter')->name('user.filter');

/** Página de listagem dos usuários */
Route::get('/list', 'UserController@index')->name('user.index');


/** Pagina de cadastrar usuários */
Route::get('/cadastrar', 'UserController@create')->name('user.create');
/** Rota de salvar usuários*/
Route::post('/cadastrar/save', 'UserController@store')->name('user.store');

/** Pagina de editar usuários */
Route::get('/editar/{id}', 'UserController@edit')->name('user.edit');
/** Rota de editar usuários*/
Route::put('/editar/update/{id}', 'UserController@update')->name('user.update');

/** Rota de deletar */
Route::get('/destroy/{id}', 'UserController@destroy')->name('user.destroy');
