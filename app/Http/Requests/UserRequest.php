<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'phone' => ['required'],
            'email' => ['required', 'unique:users,email,' . $this->id . ',id'],
            'cpf' => ['required', 'unique:users,cpf,' . $this->id . ',id']
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'O campo Nome é obrigatório!',
            'phone.required' => 'O campo Telefone é obrigatório!',
            'email.required' => 'O campo E-mail é obrigatório!',
            'cpf.required' => 'O campo CPF é obrigatório!',
            'email.unique' => 'O email informado já existe no sistema!',
            'cpf.unique' => 'O CPF informado já existe no sistema!',
        ];
    }
}
