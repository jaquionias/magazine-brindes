<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a dashboard of the resource.
     *
     *
     */
    public function dashboard()
    {
        $users = User::get();
        return view('template.dashboard', [
            'filtros' => [
                'cliente' => '',
                'vendedor' => '',
                'data_inicial' => '',
                'data_final' => '',
            ],
            'users' => $users->count()
        ]);
    }

    /**
     * Display a dashboard of the resource.
     *
     *
     */
    public function index()
    {
        $users = User::orderBy('created_at', 'DESC')->paginate(10);
        return view('template.list', [
            'filtros' => [
                'name' => '',
                'vendedor' => '',
                'data_inicial' => '',
                'data_final' => '',
            ],
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return
     */
    public function create()
    {
        return view('template.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\UserRequest $request
     *
     */
    public function store(UserRequest $request)
    {
        $dados = $request->all();
        $response = User::create($dados);
        if ($response) {
            return redirect()->route('user.index')->with([
                'message' => 'Usuário cadastrado com sucesso!'
            ]);
        }
    }

    public function filter(Request $request)
    {
        $filtros = $request->all();
        $users = User::where(function ($q) use ($filtros) {
            if (isset($filtros['cliente'])) {
                $q->where('cliente', 'like', "%" . $filtros['cliente'] . "%");
            }
            if (isset($filtros['vendedor'])) {
                $q->where('vendedor', 'like', "%" . $filtros['vendedor'] . "%");
            }
            if (isset($filtros['data_inicial'])) {
                $q->where('created_at', '>=', $this->convertStringToDate($filtros['data_inicial']));
            }
            if (isset($filtros['data_final'])) {
                $q->where('created_at', '<=', ($this->convertStringToDate($filtros['data_final']) . ' 23:59:59'));
            }
        })->orderBy('created_at', 'DESC')->paginate(1000);

        return view('template.list', [
            'filtros' => $request->all(),
            'users' => $users
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        return view('template.form', [
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UserRequest $request
     * @param int $id
     *
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::where('id', $id)->first();

        if (!$user->update($request->all())) {
            return redirect()->back()->withInput()->withErrors();
        }
        return redirect()->route('user.edit', [
            'id' => $user->id
        ])->with([
            'message' => 'Usuário atualizado com sucesso!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     */
    public function destroy($id)
    {
        $user = User::where('id', $id)->first();
        $user->delete();

        return redirect()->route('user.index')->with([
            'message' => 'Usuário deletado com sucesso!'
        ]);
    }

    public function convertStringToDate(?string $param)
    {
        if (empty($param)) {
            return null;
        }

        list($day, $month, $year) = explode('-', $param);

        return (new \DateTime($year . '-' . $month . '-' . $day))->format('Y-m-d');
    }
}
