@extends('template.master')


@section('content')
    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-12" style="display: flex; flex-direction: row; align-items: center; justify-content: center;">
                <img class="logo" style="margin-right: 20px" width="100" src="{{asset('./img/logo.png')}}" alt="">
                @if(isset($user->id))
                    <h1>Edição de Usuário </h1>
                @else
                    <h1>Cadastro de Usuário </h1>
                @endif

            </div>
        </div>
        @if($errors->all())
            @foreach($errors->all() as $error)
                <div class="alert alert-danger" role="alert">
                    {{$error}}
                </div>
            @endforeach
        @endif
        @if(session()->exists('message'))
            <div class="alert alert-success" role="alert">
                {{session()->get('message')}}
            </div>
        @endif
        <form action="{{isset($user->id) ? route('user.update', ['id' => $user->id]) : route('user.store')}}" method="post">
            @csrf
            @if(isset($user->id))
                @method('PUT')
            @endif

            <div class="form-group">
                <div class="row">

                    <div class="col-lg-8 col-sm-12">
                        <label for="name">Nome</label>
                        <input value="{{old('name') ?? $user->name ?? ''}}" type="text" placeholder="Ex.: Fulano de tal" name="name" id="name"
                               class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}">
                        @if($errors->has('name'))
                            <div class="invalid-feedback">
                                {{$errors->first('name')}}
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        <label for="phone">Telefone</label>
                        <input value="{{old('phone') ?? $user->phone ?? ''}}" type="tel" placeholder="(11) 91111-1111" name="phone" id="phone"
                               class="form-control phone {{$errors->has('phone') ? 'is-invalid' : ''}}">
                        @if($errors->has('phone'))
                            <div class="invalid-feedback">
                                {{$errors->first('phone')}}
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        <label for="email">E-mail</label>
                        <input value="{{old('email') ?? $user->email ?? ''}}" type="email" placeholder="fulano@email.com" name="email" id="email"
                               class="form-control {{$errors->has('email') ? 'is-invalid' : ''}}">
                        @if($errors->has('email'))
                            <div class="invalid-feedback">
                                {{$errors->first('email')}}
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        <label for="cpf">CPF</label>
                        <input value="{{old('cpf') ?? $user->cpf ?? ''}}" type="text" placeholder="000.000.000-00" name="cpf" id="cpf"
                               class="form-control cpf {{$errors->has('cpf') ? 'is-invalid' : ''}}">
                        @if($errors->has('cpf'))
                            <div class="invalid-feedback">
                                {{$errors->first('cpf')}}
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-4 col-sm-12">
                        <label for="birthday">Aniversário</label>
                        <input value="{{old('birthday') ?? $user->birthday ?? ''}}" type="date" placeholder="00/00/0000" name="birthday" id="birthday"
                               class="form-control {{$errors->has('birthday') ? 'is-invalid' : ''}}">
                        @if($errors->has('birthday'))
                            <div class="invalid-feedback">
                                {{$errors->first('birthday')}}
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-12 mt-3">
                        <label for="message">Mensagem</label>
                        <textarea placeholder="Mensagem" name="message" id="message" cols="30" rows="5"
                                  class="form-control  {{$errors->has('message') ? 'is-invalid' : ''}}">{{old('message') ?? $user->message ?? ''}}</textarea>
                        @if($errors->has('message'))
                            <div class="invalid-feedback">
                                {{$errors->first('message')}}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 mt-3">
                        <button type="submit" style="width: 100%" class="btn btn-success">Salvar</button>
                    </div>
                    <div class="col-6 mt-3">
                        <a href="{{route('user.dashboard')}}" style="width: 100%"  class="btn btn-info ">Dashboard</a>
                    </div>

                </div>

            </div>
        </form>

    </div>
@endsection
