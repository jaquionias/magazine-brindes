@extends('template.master')

@section('content')
    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-12" style="display: flex; flex-direction: row; align-items: center; justify-content: center;">
                <img class="logo" style="margin-right: 20px" width="100" src="{{asset('./img/logo.png')}}" alt="">
                <h1>Magazine Brindes - Dashboard</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header text-center">
                        <h2>Quant. de usuários cadastrados</h2>
                    </div>
                    <div class="card-body text-center">
                        <h2>{{$users}}</h2>
                    </div>
                </div>

            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-6 mb-6 text-right">
                <a href="{{route('user.create')}}" style="width: 100%" class="btn btn-success">Novo Usuário</a>
            </div>

            <div class="col-6 mb-6 text-right">
                <a href="{{route('user.index')}}" style="width: 100%" class="btn btn-warning">Listagem de Usuário</a>
            </div>
        </div>

    </div>
@endsection
@section('scripts')
    <script language="Javascript">
        function confirmacao(id) {
            var resposta = confirm("Realmente deseja remover esse registro?");
            if (resposta == true) {
                window.location.href = "destroy/" + id;
            }
        }
    </script>
@endsection
