<!doctype html>
<html lang="pt_br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Magazine Brindes</title>

    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>

@yield('content')

@yield('scripts')
<script src="{{asset('js/app.js')}}"></script>
<script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"
        integrity="sha256-u7MY6EG5ass8JhTuxBek18r5YG6pllB9zLqE4vZyTn4="
        crossorigin="anonymous"></script>

<script>
    //Input mask
    $(document).ready(function($){
        $('.date').mask('00/00/0000');
        $('.phone').mask('(00) 00000-0000');
        $('.cpf').mask('000.000.000-00', {reverse: true});
    });
</script>
</body>
</html>
