@extends('template.master')

@section('content')
    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-12" style="display: flex; flex-direction: row; align-items: center; justify-content: center;">
                <img class="logo" style="margin-right: 20px" width="100" src="{{asset('./img/logo.png')}}" alt="">
                <h1>Listagem de Usuários </h1>
            </div>
        </div>
        <div class="row">
            @if(session()->exists('message'))
                <div class="col-12 alert text-center alert-success" role="alert">
                    {{session()->get('message')}}
                </div>
            @endif
        </div>
        <div class="row text-right">
            <div class="col-6 ">
                <a href="{{route('user.dashboard')}}" style="width: 100%" class="btn btn-info ">Dashboard</a>
            </div>
            <div class="col-6 text-right">
                <a href="{{route('user.create')}}" style="width: 100%" class="btn btn-success">Novo Usuário</a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <form action="{{route('user.filter')}}" method="post">
                    @csrf
                    <div class="form-group">

                    </div>
                </form>
            </div>

        </div>
        <div class="row">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Telefone</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">CPF</th>
                    <th scope="col">Data de nascimento</th>
                    <th scope="col">Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->phone}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->cpf}}</td>
                        <td>{{ $user->birthday !== null ? date( 'd/m/Y' , strtotime($user->birthday)) : 'Não Informada'}}</td>
                        <td>
                            @if(isset($user->message))
                                <a class="accordion-toggle collapsed" id="accordion{{$user->id}}"
                                   data-toggle="collapse" data-parent="#accordion{{$user->id}}"
                                   href="#collapse{{$user->id}}">Mensagem | </a>
                            @endif
                            <a href="{{route('user.edit',['id' => $user->id])}}"> Editar</a>
                            <a href="javascript:func()" onclick="confirmacao('{{$user->id}}')"> | Excluir</a>
                        </td>
                    </tr>
                    @if(isset($user->message))
                        <tr class="hide-table-padding">
                            <td colspan="5" style="max-width: 100px; border:none">
                                <div class="row">
                                    <div class="col-12">
                                        <div style="background: rgba(255,255,255,1);" id="collapse{{$user->id}}" class="collapse in p-0">
                                            <p style="padding: 10px">
                                                <strong>Mensagem: </strong>
                                            </p>
                                            <p style="padding: 10px">{{$user->message}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endif
                @endforeach

                </tbody>
                <tfoot>
                {{$users}}

                </tfoot>
            </table>
        </div>
    </div>
@endsection
@section('scripts')
    <script language="Javascript">
        function confirmacao(id) {
            var resposta = confirm("Realmente deseja remover esse registro?");
            if (resposta == true) {
                window.location.href = "destroy/" + id;
            }
        }
    </script>
@endsection
